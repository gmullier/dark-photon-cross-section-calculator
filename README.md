# Dark Photon Cross Section calculator

  * [Manual for the python CLI](#manual-for-the-python-cli)
    + [Pre-requisite](#pre-requisite)
    + [Quick start example](#quick-start-example)
      - [Single plot](#single-plot)
        * [List of options](#list-of-options)
      - [Multi Plot](#multi-plot)
        * [List of options](#list-of-options-1)
  * [Manual for the Jupyter notebook](#manual-for-the-jupyter-notebook)
    + [Installing the Jupyter notebook](#installing-the-jupyter-notebook)
  * [Articles & other sources of informations used](#articles---other-sources-of-informations-used)

**Disclaimer : Work in progress.**

## Manual for the python CLI

### Pre-requisite

This needs to be run under python 3> with the following packages

- matplotlib>=3.0.3
- numpy>=1.16.2
- scipy>=1.2.1

if one of the packages not installed please run the appropriate following commands

```
pip3 install matplotlib
pip3 install numpy
pip3 install scipy
```

if you prefer virtualenv be my guest (if there is popular demand I mgiht consider adding a how to here)

### Quick start example
#### Single plot

Running the command

```
python3 DarkPhoton.py -D
```

Will display the cross section for an electron beam energy of 4GeV on a Tungsten target computed for 1000 $`A'`$ masses from 0.001 GeV to 3 GeV

```
python3 DarkPhoton.py -DN
```

Will display the cross section and number of events for an electron beam energy of 4GeV on a Tungsten target of thickness 0.1, computed for 1000 $`A'`$ masses from 0.001 GeV to 3 GeV


To change the beam energy use the -E option, for a 8 GeV electron beam one would use

```
python3 DarkPhoton.py -DN -E 8
```

In order to save the displayed graphs use the -S option, the following command

```
python3 DarkPhoton.py -DNS
```

Will display and save the cross section and number of events for an electron beam energy of 4GeV on a Tungsten target of thickness 0.1, computed for 1000 $`A'`$ masses from 0.001 GeV to 3 GeV

To change the target enter its atomic number and mass, if one compute the number of events, the radiation length is computed accordingly, though this is an approximation, one can enter their own radiation length by using the -R option and target thickness using the -T option

For an aluminum target one would enter the following

```
python3 DarkPhoton.py -D -Z 13 -A 26.98
```

##### List of options

Option short | Option Long         | Description                                                          | Default value   |
------------ | ------------------- | -------------------------------------------------------------------- | --------------- |
-N           | --NEvents           | Activate the Number of events                                        | False           |
-D           | --Display           | Display Plot                                                         | False           |
-S           | --SaveOutput        | Write file                                                           | False           |
-V           | --Verbose           | Verbose plot output                                                  | False           |
-O           | --OutputDir         | Path to the directory to store the plot or subdirectory              | "Output/"       |
-Z           | --AtomicNumber      | Target Z, if not defined, default to Tungsten                        | 74              |
-A           | --MassNumber        | Target A, if not defined, default to Tungsten                        | 183.84          |
-e           | --ElectronsOnTarget | Electrons on target in scientific notation                           | 4e14            |
-R           | --RadLength         | Radiation Length in g/cm^2, If not specified computed automatically  | Computed        |
-T           | --Thickness         | Target thickness in Rad length                                       | 0.1             |
-1           | --Order1            | Term of order 1                                                      | 1               |
-F           | --Factor            | Scaling factor                                                       | 1               |
-E           | --BeamEnergy        | Beam Energy in GeV                                                   | 4               |
-K           | --KineticParameter  | Kinematic parameter, Epsilon                                         | 1               |
-m           | --minMass           | Minimum mass of A' in GeV                                            | 0.001           |
-M           | --maxMass           | Maximum mass of A' in GeV                                            | 3               |
-P           | --NumberofPoints    | Number of points where the cross section is computed                 | 1000            |


#### Multi Plot


```
python3 DarkPhotonMulti.py -E 4,8 -Z [74,183.84] -D
```

Will display the cross section for an electron beam energy of 4GeV and 8GeV on a Tungsten target computed for 1000 $`A'`$ masses from 0.001 GeV to 3 GeV

```
python3 DarkPhotonMulti.py -E 4,8 -Z [74,183.84] -DS
```

Will display and save to pdf

```
python3 DarkPhotonMulti.py -E 4,8 -Z [74,183.84] -H
```

Will save display a HTML version of the plot

For multiple elements type them in a list fashion

```
python3 DarkPhotonMulti.py -E 4,8 -Z [74,183.84],[13,26.98],[22,47.86] -D
```

##### List of options

Option short | Option Long         | Description                                                          | Default value   |
------------ | ------------------- | -------------------------------------------------------------------- | --------------- |
-D           | --Display           | Display Plot                                                         | False           |
-S           | --SaveOutput        | Write file                                                           | False           |
-R           | --Ratio             | Compute Ratio plot in-between E1 [Z1,A1] and E2 [Z2,A2]...           | False           |
-O           | --OutputDir         | to the directory to store the plot or subdirectory                   | "Output/"       |
-Z           | --AtomicNumber      | List of targets Z and A in the format [Z1,A1],[Z2,A2],...            | null            |
-H           | --HTML              | Create HTML plot                                                     | False           |
-1           | --Order1            | Term of order 1                                                      | 1               |
-F           | --Factor            | Scaling factor                                                       | 1               |
-E           | --BeamEnergy        | List of Beam Energy in GeV in the format E1,E2,E3,...                | null            |
-K           | --KineticParameter  | Kinematic parameter, Epsilon                                         | 1               |
-m           | --minMass           | Minimum mass of A' in GeV                                            | 0.001           |
-M           | --maxMass           | Maximum mass of A' in GeV                                            | 3               |
-P           | --NumberofPoints    | Number of points where the cross section is computed                 | 1000            |


---
## Manual for the Jupyter notebook

### Installing the Jupyter notebook

One can follow installation instructions the Jupyter notebook from there https://jupyter.org/install or jupyterlab if you like to live dangerously https://github.com/jupyterlab/jupyterlab

NB: 
Conda highly recommended for the installation, though if you are like me a "420 YOLO swag blaze it 360 noscope package mayhem 👌" kind of person or your pythonFu is strong, you might want to go the pip way.

The Kernel needs to be python3, if installed with Conda all the dependencies should be installed but for one, plotly (I think, but don't quote me on it)

Plotly is deactivated by default, just change the relevant cells from 'RAW' to 'code'

---

## Articles & other sources of informations used

- https://arxiv.org/abs/0906.0580 New Fixed-Target Experiments to Search for Dark Gauge Forces Appendix A and in particular
    - Definition of $`\beta_{A'}`$ (annotated only by $`\beta`$ by me) (Right after equation A12)
    - The definition of $`\chi`$ (A17)
    - Use especially eq A17, A18, A19
    - parameters $`a`$, $`d`$, $`a'`$, $`\mu_p`$
    - Fig 10 $`\left(\frac{\chi}{Z^2}\right)`$ was used as cross check (the code can be quickly adapted to plot those quantities too if you so desire)
- https://arxiv.org/abs/1411.1404 Testing GeV-Scale Dark Matter with Fixed-Target Missing Momentum Experiments
    - approximation of the cross section which is eq 8
    - $`\delta`$ is defined in equation 7 
    - Fig 5 is the one that I use as a reference 

Furthermore the radiation lengths, in g/cm$`^2`$, can be found at the following link http://pdg.lbl.gov/2018/AtomicNuclearProperties/ more precisely Tungsten here http://pdg.lbl.gov/2018/AtomicNuclearProperties/HTML/tungsten_W.html Aluminium there http://pdg.lbl.gov/2018/AtomicNuclearProperties/HTML_PAGES/aluminum_Al.html

Though the default behaviour is not using a Lookup table with precise values but a quick estimate of the radiation length by the following formula that can have up to 3% error, but usually <1%, be wary of this.

$`X_0 = \frac {716.4}{ Z (Z+1) \ln\left(\frac{287}{\sqrt{Z}}\right)} A`$

---
Cross section approximation

$`\sigma_{A'} \approx F\ \frac{4}{3}\ \frac{\alpha^3\epsilon^2}{{m^2_{A'}}}\ \sqrt{1-{\left(\frac{m_{A'}}{E_0}\right)}^2}\chi_{(Z,m_{A'},E_0)}\left[\ln(\delta^{-1})+O(1)\right]`$

Definition of $`\delta`$

$`\delta \equiv \max\left(\frac{m_{A'}}{E_0},\frac{m^2_e}{m^2_{A'}},\frac{m_e}{E_0} \right )`$

Nuclear form factor

$`\chi \equiv \int_{t_m}^{t_M}\left(\frac{t-t_m}{t^2}\right) \left(G_{2,in(t)} + G_{2,el(t)}\right){dt}`$

Nuclear form factor elastic component

$`\chi_{el} \equiv \int_{t_m}^{t_M}\left(\frac{t-t_m}{t^2}\right) \left(\frac{a^2t}{1+a^2t}\right)^2 \left(\frac{1}{1+\frac{t}{d}}\right)^2 Z^2\ {dt}`$

Nuclear form factor inelastic component

$`\chi_{in} \equiv \int_{t_m}^{t_M}\left(\frac{t-t_m}{t^2}\right) \left(\frac{a'^2t}{1+a'^2t}\right)^2 \left(\frac{1+\frac{t}{4m_p^2}\left(\mu_p^2-1\right)}{(1+\frac{t}{0.71})^4}\right)^2 Z\ {dt}`$

Minimum of momentum transfer

$`t_m \equiv  \left(\frac{m^2_{A'}}{2E_0}\right)^2`$

Maximum of momentum transfer

$`t_M \equiv  m^2_{A'}`$

$`a\equiv \frac{111}{m_e}\ Z^{-\frac{1}{3}}`$

$`d\equiv 0.164\ A^{-\frac{2}{3}}`$

$`a'\equiv \frac{773}{m_e}\ Z^{\frac{1}{3}}`$

$`\mu_p\equiv 2.79`$

$`m_p\equiv 0.938272`$

$`\alpha \equiv \frac{1}{137}`$