#Approximate cross section in function of energy

#Checking that Python is at least version 3.0
import sys
if sys.version_info[0] < 3:
    print('You need to run this with Python 3!')
    sys.exit(1)


import pkg_resources
from pkg_resources import DistributionNotFound, VersionConflict

# Checking if dependencies are met
dependencies = [
'matplotlib>=3.0.3',
'numpy>=1.16.2',
'scipy>=1.2.1'
]
# if dependency not met, exception is thrown. 
pkg_resources.require(dependencies)



import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import scipy.special as special

#Checking for extra features
# import importlib
# if importlib.util.find_spec('plotly'):
#     import plotly
# else:
#     print('Plotly not found, Deactivating html plotting')

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-N" , "--NEvents"           , action='store_true'                                 , help="Activate the Number of events"             , default=False)
parser.add_option("-D" , "--Display"           , action='store_true'                                 , help="Display Plot"                              , default=False)
parser.add_option("-S" , "--SaveOutput"        , action='store_true'                                 , help="Write file"                                , default=False)
parser.add_option("-V" , "--Verbose"           , action='store_false'                                , help="Verbose plot output"                       , default=True)
parser.add_option("-O" , "--OutputDir"         , help="Full path to the directory to store the plot" , default="Output/")
parser.add_option("-Z" , "--AtomicNumber"      , type=float                                          , help="Target Z                                   , default to Tungsten   , 74"              , default="74")
parser.add_option("-A" , "--MassNumber"        , type=float                                          , help="Target A                                   , default to Tungsten   , 183.84 "         , default="183.84")
parser.add_option("-e" , "--ElectronsOnTarget" , type=float                                          , help="Electrons on target in scientific notation , defautls to 4e14 "                       , default="4e14")
parser.add_option("-R" , "--RadLength"         , type=float                                          , help="Radiation Length in g/cm^2                 , If not specified computed automatically" , default="-1")
parser.add_option("-T" , "--Thickness"         , type=float                                          , help="Target thickness in Rad length             , default to LDMX specs , 0.1"             , default="-1")
parser.add_option("-1" , "--Order1"            , type=float                                          , help="Term of order 1                            , defautls to 1"                           , default="1")
parser.add_option("-F" , "--Factor"            , type=float                                          , help="Scaling factor                             , defaults to 1"                           , default="1")
parser.add_option("-E" , "--BeamEnergy"        , type=float                                          , help="Beam Energy in GeV                         , defaults to 4"                           , default="4")
parser.add_option("-K" , "--KineticParameter"  , type=float                                          , help="Kinematic parameter                        , Epsilon , defaults to 1"                 , default="1")
parser.add_option("-m" , "--minMass"           , type=float                                          , help="Minimum mass of A' in GeV                  , defautls to 0.001"                       , default="0.001")
parser.add_option("-M" , "--maxMass"           , type=float                                          , help="Maximum mass of A' in GeV                  , defautls to 3"                           , default="3")
parser.add_option("-P" , "--NumberofPoints"    , type=int                                            , help="Number of points used for the plotting     , defaults to 1000"                        , default="1000")

(options, args) = parser.parse_args()


#Informations on the target
#Atomic number
Z = options.AtomicNumber
# Z=13
#Atomic mass
A = options.MassNumber
# A=26.98
#Energy of the incoming beam in GeV
# E_0=8
E_0 = options.BeamEnergy
#Value of alpha
Alpha=1/137
#mass of the electron in GeV
m_e = 0.000510998
#proton mass in GeV
m_p = 0.938272
#Arbitrary value of the kinetic parameter can vary from 10^-2 to 10^-8
# Epsilon = 10^(-5)
Epsilon = options.KineticParameter
#Avogadro number
N_A = 6.022140857*(10**23)


def RadiationLengthEstimation(Z,A):
    X_0 = ( 716.4 / ( Z * (Z+1) * np.log(287/np.sqrt(Z) ))) * A
    return X_0


#Number of electrons on target
NeTarget = options.ElectronsOnTarget
#Radiation length g/ cm^2
if options.RadLength > 0:
    if options.NEvents:
        print('Using provided radiation lenght')
        RadLength = options.RadLength
else:
    if options.NEvents:
        RadLength = RadiationLengthEstimation(Z,A)
        print('Approximating radiation length from atomic number RadLength = '+str(RadLength)+' g/cm^2, use to your own risk...')
#Length of material in radiation length
if options.NEvents:
    if options.Thickness > 0:
        T = options.Thickness
        print('Using thickness in radiation length provided T = ' +str(T))
    else:
        T = 0.1
        print('No Thickness provided, using default thickness in radiation lenghts of ' +str(T))
else:
    T = 0
#Just to get the correct scaling of sigma
SigmaUnits = 10**(-27)

#Homemade correction factors
CorrectionFactor = options.Factor
SomethingOfOrder1= options.Order1

#Estimation of the integral of the elastic term of Chi
a=111*(Z**(-1/3))/m_e
d=0.164*(A**(-2/3))
def IntegralElastic(t, a, d, Z,tmin):
    return ((t-tmin)/(t**2)) * (((a**2*t)/(1+(a**2*t)))**2) * ((1/(1+(t/d)))**2) * (Z**2)

a_prime=773*(Z**(-2/3))/m_e
mu_p=2.79
def IntegralInelastic(t, a_prime, m_p, mu_p, Z,tmin):
    return ((t-tmin)/(t**2)) * ((((a_prime**2)*t)/(1+((a_prime**2)*t)))**2) * (((1+((t)/(4*(m_p**2))*(mu_p**2-1)))/(1+(t/0.71))**4)**2) * (Z)




SigmaValue=[]
EnergyValue=[]



#Mass iteration in GeV
Mass_A_Min= options.minMass
Mass_A_Max= options.maxMass
Mass_A_Numberofpoints=options.NumberofPoints
SigmaValue=[]
MassValue=[]
EventsValue=[]

if not(options.Display) and not(options.SaveOutput):
    print("You decided to do nothing, leaving before making useless computations... You might want to turn on the display with the -D option... And read the manual... Bye! \U0001F44B ")
    exit(0)

for Mass_A_Iteration in np.geomspace(Mass_A_Min, Mass_A_Max,Mass_A_Numberofpoints):
    #Min and max of ranges given by paper
    tmin=((Mass_A_Iteration**2)/(2*E_0))**2
    tmax=(Mass_A_Iteration**2)
    #Phi in paper is Beta*Chi
    Beta = np.sqrt(1 - ((Mass_A_Iteration/E_0)**2))
    delta = max((Mass_A_Iteration/E_0),((m_e**2)/(Mass_A_Iteration**2)),(m_e/E_0))
    factor= (4/3)*(Alpha**3)*(Epsilon**2)*Beta/(Mass_A_Iteration**2)

    #Doing the integrals and adding them (second term of the array is the error on the integral)
    Chi_Elastic = integrate.quad(IntegralElastic, tmin, tmax, args=(a, d, Z,tmin))

    Chi_Inelastic = integrate.quad(IntegralInelastic, tmin, tmax, args=(a_prime,m_p , mu_p, Z,tmin))

    Chi = Chi_Elastic[0] + Chi_Inelastic[0]

    sigma = CorrectionFactor*factor * Chi * (np.log((delta)**(-1)) + SomethingOfOrder1)
    
    SigmaValue.append(sigma*1000)
    MassValue.append(Mass_A_Iteration*1000)


    #Computing Yield for target of radiation length RadLength, length T in radiation length and a luminosity
    if options.NEvents:
        Nevents = sigma * SigmaUnits * (RadLength*T*NeTarget*N_A/(A))
        EventsValue.append(Nevents)

fig, ax = plt.subplots()
plt.figure(1)
plt.plot(MassValue,SigmaValue,'ro',markersize=1)
plt.yscale('log')
plt.xscale('log')
# ax.set_yticks([10**(-4),10**(-3),10**(-2),10**(-1),10**0,10**1,10**2,10**3,10**4,10**5,10**6,10**7])
plt.tick_params(axis='both')

plt.ylim(top=max(SigmaValue)*1.2)
plt.ylim(bottom=min(SigmaValue)*0.8)
plt.xlim(right=max(MassValue)*1.2)
plt.xlim(left=min(MassValue)*0.8)

if options.NEvents:
    plt.figure(2)
    plt.plot(MassValue,EventsValue,'ro',markersize=1)
    plt.yscale('log')
    plt.xscale('log')
    # ax.set_yticks([10**(0),10**(1),10**(2),10**(3),10**4,10**5,10**6,10**7,10**8,10**9,10**10,10**11])
    plt.tick_params(axis='both')
    
    plt.ylim(top=max(EventsValue)*1.2)
    plt.ylim(bottom=min(EventsValue)*0.8)
    plt.xlim(right=max(MassValue)*1.2)
    plt.xlim(left=min(MassValue)*0.8)

if options.Verbose:
    plt.figure(1)
    plt.title('Dark photon cross section\n Target Z = '+str(int(Z)) +', A = '+str(A)+' | Beam energy = '+str(E_0)+' GeV')
    if options.NEvents:
        plt.figure(2)
        plt.title('Dark photon Events EOT = '+'{:.2e}'.format(NeTarget)+'\n Target Z = '+str(int(Z)) +', A = '+str(A)+' | Beam energy = '+str(E_0)+' GeV')
else:
    plt.figure(1)
    plt.title('Dark photon cross section\n' +'$E_0 =$ '+str(E_0)+' | $\epsilon =$ ' +str(Epsilon)+' | F = '+str(CorrectionFactor)+' | $O(1)$ = '+str(SomethingOfOrder1) +' | $Z$ = '+str(int(Z)) +' | $A$ = '+str(A))
    if options.NEvents:
        plt.figure(2)
        plt.title('Dark photon Events section EOT = '+'{:.2e}'.format(NeTarget)+'\n' +'$E_0 =$ '+str(E_0)+' | $\epsilon =$ ' +str(Epsilon)+' | F = '+str(CorrectionFactor)+' | $O(1)$ = '+str(SomethingOfOrder1) +' | $Z$ = '+str(int(Z)) +' | $A$ = '+str(A) +' | $T$ = '+str(T) )

plt.figure(1)
plt.xlabel('$m_{A\'}$ [MeV]')
plt.ylabel('$\sigma$ [$\mu$b] ')
plt.grid(color='grey', linestyle='-', linewidth=0.5,alpha=0.3,which="both")

if options.NEvents:
    plt.figure(2)
    plt.xlabel('$m_{A\'}$ [MeV]')
    plt.ylabel('$\#$  Events')
    plt.grid(color='grey', linestyle='-', linewidth=0.5,alpha=0.3,which="both")

if options.SaveOutput:
    plt.figure(1)
    plt.savefig(options.OutputDir+'XSec_DarkPhoton_E0_'+str(E_0).replace('.','_')+'_Epsilon_'+str(Epsilon).replace('.','_')+'_F_'+str(CorrectionFactor).replace('.','_')+'_O1_'+str(SomethingOfOrder1).replace('.','_')+'_Z_'+str(int(Z)).replace('.','_')+'_A_'+str(A).replace('.','_')+'.pdf', format='pdf')
    print('File Saved : '+ options.OutputDir+'XSec_DarkPhoton_E0_'+str(E_0).replace('.','_')+'_Epsilon_'+str(Epsilon).replace('.','_')+'_F_'+str(CorrectionFactor).replace('.','_')+'_O1_'+str(SomethingOfOrder1).replace('.','_')+'_Z_'+str(int(Z)).replace('.','_')+'_A_'+str(A).replace('.','_')+'.pdf')
    if options.NEvents:
        plt.figure(2)
        plt.savefig(options.OutputDir+'NEvents_DarkPhoton_E0_'+str(E_0).replace('.','_')+'_Epsilon_'+str(Epsilon).replace('.','_')+'_F_'+str(CorrectionFactor).replace('.','_')+'_O1_'+str(SomethingOfOrder1).replace('.','_')+'_T_'+str(T).replace('.','_')+'_Z_'+str(int(Z)).replace('.','_')+'_A_'+str(A).replace('.','_')+'.pdf', format='pdf')
        print('File Saved : '+ options.OutputDir+'NEvents_DarkPhoton_E0_'+str(E_0).replace('.','_')+'_Epsilon_'+str(Epsilon).replace('.','_')+'_F_'+str(CorrectionFactor).replace('.','_')+'_O1_'+str(SomethingOfOrder1).replace('.','_')+'_T_'+str(T).replace('.','_')+'_Z_'+str(int(Z)).replace('.','_')+'_A_'+str(A).replace('.','_')+'.pdf')
        
if options.Display:
    print("Display enabled")
    plt.show()


exit(0)