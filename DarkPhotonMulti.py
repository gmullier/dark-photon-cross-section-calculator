#Approximate cross section in function of energy

#Checking that Python is at least version 3.0
import sys
if sys.version_info[0] < 3:
    print('You need to run this with Python 3!')
    sys.exit(1)


import pkg_resources
from pkg_resources import DistributionNotFound, VersionConflict

# Checking if dependencies are met
dependencies = [
'matplotlib>=3.0.3',
'numpy>=1.16.2',
'scipy>=1.2.1'
]
# if dependency not met, exception is thrown. 
pkg_resources.require(dependencies)

import ast
from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import scipy.special as special

#Checking for extra features
import importlib
import types

if importlib.util.find_spec('plotly'):
    import plotly
    import plotly
    import plotly.plotly as py
    import plotly.figure_factory as FF
    from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
    import plotly.io as pio
    import plotly.graph_objs as go
else:
    print('Plotly not found, Deactivating html plotting')

from optparse import OptionParser

def MyListParser(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))


parser = OptionParser()
parser.add_option("-D" , "--Display"          , action='store_true'                                 , help="Display Plot"                          , default=False)
parser.add_option("-S" , "--SaveOutput"       , action='store_true'                                 , help="Write file"                            , default=False)
parser.add_option("-R" , "--Ratio"            , action='store_true'                                 , help="Ratio plot"                            , default=False)
parser.add_option("-O" , "--OutputDir"        , help="Full path to the directory to store the plot" , default="Output/")
parser.add_option("-Z" , "--AtomicNumber"     , type='string'                                       , help="List of taget elements in Atomic number, Atomic Mass couple pass arguments as\"[Z1,A1],[Z2,A2],...\"")
parser.add_option("-H" , "--HTML"             , action='store_true'                                 , help="Create HTML plot"                      , default=False)
parser.add_option("-1" , "--Order1"           , type=float                                          , help="Term of order 1                        , defautls to 1"           , default="1")
parser.add_option("-F" , "--Factor"           , type=float                                          , help="Scaling factor                         , defaults to 1"           , default="1")
parser.add_option("-E" , "--BeamEnergy"       , type='string'                                       , default=""                                   , action='callback'        , callback=MyListParser , help="List of Beam energies, pass arugment as E1,E2,E3")
parser.add_option("-K" , "--KineticParameter" , type=float                                          , help="Kinematic parameter                    , Epsilon                  , defaults to 1"        , default="1")
parser.add_option("-m" , "--minMass"          , type=float                                          , help="Minimum mass of A' in GeV              , defautls to 0.001"       , default="0.001")
parser.add_option("-M" , "--maxMass"          , type=float                                          , help="Maximum mass of A' in GeV              , defautls to 3"           , default="3")
parser.add_option("-P" , "--NumberofPoints"   , type=int                                            , help="Number of points used for the plotting , defaults to 1000"        , default="1000")

(options                 , args) = parser.parse_args()

#Convert CLI input to the right type in order to make the computations needed
BeamEnergyList = [float(i) for i in options.BeamEnergy]

TargetList=[]
if isinstance(ast.literal_eval(options.AtomicNumber), list):
    TargetList.append(ast.literal_eval(options.AtomicNumber))
else:
    TargetList = list(ast.literal_eval(options.AtomicNumber))



(options, args) = parser.parse_args()

#Value of alpha
Alpha=1/137
#mass of the electron in GeV
m_e = 0.000510998
#proton mass in GeV
m_p = 0.938272
#Arbitrary value of the kinetic parameter can vary from 10^-2 to 10^-8
# Epsilon = 10^(-5)
Epsilon = options.KineticParameter
#Avogadro number
N_A = 6.022140857*(10**23)

#Just to get the correct scaling of sigma
SigmaUnits = 10**(-27)

#Homemade correction factors
CorrectionFactor = options.Factor
SomethingOfOrder1= options.Order1

#Estimation of the integral of the elastic term of Chi

def IntegralElastic(t, Z, A,tmin):
    a=111*(Z**(-1/3))/m_e
    d=0.164*(A**(-2/3))
    return ((t-tmin)/(t**2)) * (((a**2*t)/(1+(a**2*t)))**2) * ((1/(1+(t/d)))**2) * (Z**2)


def IntegralInelastic(t, m_p, Z,tmin):
    a_prime=773*(Z**(-2/3))/m_e
    mu_p=2.79
    return ((t-tmin)/(t**2)) * ((((a_prime**2)*t)/(1+((a_prime**2)*t)))**2) * (((1+((t)/(4*(m_p**2))*(mu_p**2-1)))/(1+(t/0.71))**4)**2) * (Z)


#Mass iteration in GeV
Mass_A_Min= options.minMass
Mass_A_Max= options.maxMass
Mass_A_Numberofpoints=options.NumberofPoints
SigmaValue=[]
MassValue=[]
EnergyValue=[]
SigmaEnergy=[]
MassEnergy=[]
PlottingElement=[]
PlottingList=[]

if not(BeamEnergyList) or not(TargetList):
    print("Needs to have at least one beam energy and one Target Element, aborting computation")
    exit(0)

if not(options.Display) and not(options.SaveOutput) and not(options.HTML):
    print("You decided to do nothing, leaving before making useless computations... You might want to turn on the display with the -D option... And read the manual... Bye! \U0001F44B ")
    exit(0)
#Looping over the targets
for TargetElement in TargetList:
    #Looping on the enrgies
    for BeamEnergy in BeamEnergyList:
        PlottingElement=[]
        SigmaValue=[]
        MassValue=[]
        #Looping over the masses
        for Mass_A_Iteration in np.geomspace(Mass_A_Min, Mass_A_Max,Mass_A_Numberofpoints):
            #Min and max of ranges given by paper
            tmin=((Mass_A_Iteration**2)/(2*BeamEnergy))**2
            tmax=(Mass_A_Iteration**2)
            #Phi in paper is Beta*Chi
            Beta = np.sqrt(1 - ((Mass_A_Iteration/BeamEnergy)**2))
            delta = max((Mass_A_Iteration/BeamEnergy),((m_e**2)/(Mass_A_Iteration**2)),(m_e/BeamEnergy))
            factor= (4/3)*(Alpha**3)*(Epsilon**2)*Beta/(Mass_A_Iteration**2)
        
            #Doing the integrals and adding them (second term of the array is the error on the integral)
            Chi_Elastic = integrate.quad(IntegralElastic, tmin, tmax, args=(TargetElement[0],TargetElement[1],tmin))
            Chi_Inelastic = integrate.quad(IntegralInelastic, tmin, tmax, args=(m_p, TargetElement[0],tmin))
        
            Chi = Chi_Elastic[0] + Chi_Inelastic[0]
        
            sigma = CorrectionFactor*factor * Chi * (np.log((delta)**(-1)) + SomethingOfOrder1)
            
            SigmaValue.append(sigma*1000)
            MassValue.append(Mass_A_Iteration*1000)
        
        PlottingElement.append(MassValue)
        PlottingElement.append(SigmaValue)
        PlottingElement.append(BeamEnergy)
        PlottingElement.append(TargetElement)
        # PlottingElement.append(TargetList)
        PlottingList.append(PlottingElement)
# print(SigmaEnergy)
# print(MassEnergy)

#cycling automatically in-between line styles and line colours
default_cycler = (cycler(color=['r', 'b', 'y', 'c', 'k']) * cycler(linestyle=['-', '--', ':']))
plt.rc('lines', linewidth=1)
plt.rc('axes', prop_cycle=default_cycler)

fig, ax = plt.subplots()

plt.figure(1)
#AllSigmasd there to find which is the max and min to allow to have autorange corrected by a factor to display things in a more pleasing way
AllSigmas=[]
#Looping over the lists of cross sections
for ThingsToPlot in PlottingList:
    plt.plot(ThingsToPlot[0],ThingsToPlot[1],markersize=1,label='$E_0$ = '+str(ThingsToPlot[2])+' GeV\n'+' $Z$ = '+str(ThingsToPlot[3][0])+'\n $A$ = '+str(ThingsToPlot[3][1]))    
    AllSigmas.append(ThingsToPlot[1])

FlatSigmas = [item for sublist in AllSigmas for item in sublist]

plt.yscale('log')
plt.xscale('log')
# ax.set_yticks([10**(-4),10**(-3),10**(-2),10**(-1),10**0,10**1,10**2,10**3,10**4,10**5,10**6,10**7])
plt.tick_params(axis='both')
    
plt.ylim(top=max(FlatSigmas)*1.2)
plt.ylim(bottom=min(FlatSigmas)*0.8)
plt.xlim(right=max(MassValue)*1.2)
plt.xlim(left=min(MassValue)*0.8)

plt.xlabel('$m_{A\'}$ [MeV]')
plt.ylabel('$\sigma$ [$\mu$b] ')
plt.grid(color='grey', linestyle='-', linewidth=0.5,alpha=0.3,which="both")
plt.title('Dark photon cross section')
# Shrink current axis by 20%
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

# Put a legend to the right of the current axis
Legend = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.tight_layout()


if options.Ratio:
    fig1, ax1 = plt.subplots()
    plt.figure(2)
    for i in range(len(PlottingList)-1):
        plt.plot(PlottingList[i+1][0],[a/b for a,b in zip(PlottingList[i+1][1], PlottingList[0][1])],markersize=1,label='$E_0$ = '+str(PlottingList[i+1][2])+' GeV\n'+' $Z$ = '+str(PlottingList[i+1][3][0])+'\n $A$ = '+str(PlottingList[i+1][3][1]))    

    plt.xscale('log')
    plt.yscale('log')

    plt.tick_params(axis='both')

    plt.xlabel('$m_{A\'}$ [MeV]')
    plt.ylabel('Cross section ratio ')
    plt.grid(color='grey', linestyle='-', linewidth=0.5,alpha=0.3,which="both")
    plt.title('Dark photon cross section \n ratio to '+'$E_0$ = '+str(PlottingList[0][2])+' GeV'+' $Z$ = '+str(PlottingList[0][3][0])+' $A$ = '+str(PlottingList[0][3][1]))
    box1 = ax1.get_position()
    ax1.set_position([box1.x0, box1.y0, box1.width * 0.9, box1.height])

    # Put a legend to the right of the current axis
    Legend1 = ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.tight_layout()



if options.SaveOutput:
    plt.figure(1)
    plt.savefig(options.OutputDir+'XSec_DarkPhoton_Comparison_'+str(BeamEnergyList)+'_'+str(TargetList)+'.pdf', format='pdf', bbox_extra_artists=(Legend,), bbox_inches='tight',figsize=(16, 9))
    print('File Saved : '+options.OutputDir+'XSec_DarkPhoton_Comparison_'+str(BeamEnergyList)+'_'+str(TargetList)+'.pdf')
    if options.Ratio:
            plt.figure(2)
            plt.savefig(options.OutputDir+'XSec_DarkPhoton_Comparison_Ratios_'+str(BeamEnergyList)+'_'+str(TargetList)+'.pdf', format='pdf', bbox_extra_artists=(Legend1,), bbox_inches='tight',figsize=(16, 9))
            print('File Saved : '+options.OutputDir+'XSec_DarkPhoton_Comparison_Ratios_'+str(BeamEnergyList)+'_'+str(TargetList)+'.pdf')



if options.HTML:    
    if importlib.util.find_spec('plotly'):
        Traces=[]
        for ThingsToPlot in PlottingList:
            Traces.append(go.Scatter(x=ThingsToPlot[0], y=ThingsToPlot[1] , mode='lines', line=dict(), name='$E_0 = '+str(ThingsToPlot[2])+' \mbox{GeV } | '+'\ Z = '+str(ThingsToPlot[3][0])+'\mbox{ |}\ A = '+str(ThingsToPlot[3][1])+'$'))    
            # Traces.append(go.Scatter(x=ThingsToPlot[0], y=ThingsToPlot[1] , mode='lines', line=dict(color = ('rgb(230, 228, 59)')), name='$E_0 = '+str(ThingsToPlot[2])+' \mbox{GeV } | '+'\ Z = '+str(ThingsToPlot[3][0])+'\mbox{ |}\ A = '+str(ThingsToPlot[3][1])+'$'))    
            layout = go.Layout(title='$\mbox{Dark Photon Cross Section } A\'$', plot_bgcolor='rgb(0, 0,0)', 
            xaxis=dict(type='log',autorange=True,title='$m_{A\'} \mbox{[MeV]}$',titlefont=dict(family='Courier New, monospace',size=18, color='#7f7f7f')),
            yaxis=dict(type='log',autorange=True,title='$\sigma [\mu\mbox{b}]$',titlefont=dict(family='Courier New, monospace',size=18, color='#7f7f7f')))
        
        fig = go.Figure(data=Traces, layout=layout)

        # Plot data in HTML
        plot(fig,filename=options.OutputDir+'DarkPhotonCrossSection_Comparison_'+str(BeamEnergyList)+'_'+str(TargetList)+'.html', include_mathjax='cdn')

        if options.Ratio:
            TracesRatio=[]
            for i in range(len(PlottingList)-1):
                TracesRatio.append(go.Scatter(x=PlottingList[i+1][0],y=[a/b for a,b in zip(PlottingList[i+1][1], PlottingList[0][1])], mode='lines', line=dict(),name='$E_0 = '+str(PlottingList[i+1][2])+' \mbox{GeV } | '+'\ Z = '+str(PlottingList[i+1][3][0])+'\mbox{ |}\ A = '+str(PlottingList[i+1][3][1])+'$'))
            layout1 = go.Layout(title='$\mbox{Dark Photon cross section ratio to }'+ 'E_0 = '+str(PlottingList[0][2])+' \mbox{GeV } | '+'\ Z = '+str(PlottingList[0][3][0])+'\mbox{ |}\ A = '+str(PlottingList[0][3][1])+'$', plot_bgcolor='rgb(0, 0,0)', 
            xaxis=dict(type='log',autorange=True,title='$m_{A\'} \mbox{[MeV]}$',titlefont=dict(family='Courier New, monospace',size=18, color='#7f7f7f')),
            yaxis=dict(type='log',autorange=True,title='$\mbox{Ratio}$',titlefont=dict(family='Courier New, monospace',size=18, color='#7f7f7f')))
            fig1 = go.Figure(data=TracesRatio, layout=layout1)
            plot(fig1,filename=options.OutputDir+'DarkPhotonCrossSection_Comparison_Ratios'+str(BeamEnergyList)+'_'+str(TargetList)+'.html', include_mathjax='cdn')
    else:
        print("You activated the HTML option but do not have plotly, can't really help you here if it's not there...")

if options.Display:
    plt.show()

exit(0)